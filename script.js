class UserService {
    username;
    #password;
    
    constructor(username, password) {
        this.username = username;
        this.#password = password;
    }

    get username() {
        return this.username;
    }

    async authenticate_user() {
        const url = new URL("https://examples.com/api/user/authenticate");
        url.searchParams.set("username", this.username.toString());
        url.searchParams.set("password", this.#password.toString());
        
       return await fetch(url)
        .then((resp) =>  resp.status === 200 ? Promise.resolve(resp.json()) : Promise.reject(new Error(resp.statusText)))
        .catch((err) => err)
    }
}

const form = document.querySelector('.form');
const login = form.getElementById('login');

login.addEventListener("click", function() {
    const result = new UserService(username, password).authenticate_user;
    result ? window.location.href = "/" : alert(result.error);
})

